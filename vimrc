" bring in the bundles for mac and windows
let mapleader = ','
call pathogen#infect()
call pathogen#helptags()
autocmd Filetype gitcommit setlocal spell textwidth=72
set t_Co=256

" Remove trailing whitespace
autocmd BufWritePre *.py :%s/\s\+$//e

set pastetoggle=<leader>p

" if !empty($MY_RUBY_HOME)
"    let g:ruby_path = join(split(glob($MY_RUBY_HOME.'/lib/ruby/*.*')."\n".glob($MY_RUBY_HOME.'/lib/rubysite_ruby/*'),"\n"),',')
" endif
"
" ruby path if you are using rbenv
let g:ruby_path = system('echo $HOME/.rbenv/shims')

let g:loaded_netrw       = 1
let g:loaded_netrwPlugin = 1

autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

" Close all open buffers on entering a window if the only
" buffer that's left is the NERDTree buffer
function! s:CloseIfOnlyNerdTreeLeft()
  if exists("t:NERDTreeBufName")
    if bufwinnr(t:NERDTreeBufName) != -1
      if winnr("$") == 1
        q
      endif
    endif
  endif
endfunction

runtime! common_config/*.vim
runtime! custom_config/*.vim


" The Silver Searcher Config
nmap g/ :Ag!<space>
nmap g* :Ag! -w <C-R><C-W><space>
nmap ga :AgAdd!<space>
nmap gn :cnext<CR>
nmap gp :cprev<CR>
nmap gq :ccl<CR>
nmap gl :cwindow<CR>

" Tagbar Configuration
let g:tagbar_autofocus = 1
map <Leader>rt :!ctags --extra=+f -R *<CR><CR>
map <Leader>. :TagbarToggle<CR>

" Markdown Syntax Highlighting
augroup mkd
  autocmd BufNewFile,BufRead *.mkd      set ai formatoptions=tcroqn2 comments=n:> filetype=markdown
  autocmd BufNewFile,BufRead *.md       set ai formatoptions=tcroqn2 comments=n:> filetype=markdown
  autocmd BufNewFile,BufRead *.markdown set ai formatoptions=tcroqn2 comments=n:> filetype=markdown
augroup END


" Markdown Preview
map <buffer> <Leader>mp :Mm<CR>

" NERDTree for project drawer
let NERDTreeHijackNetrw = 0

nmap gt :NERDTreeToggle<CR>
nmap g :NERDTree \| NERDTreeToggle \| NERDTreeFind<CR>

" Tabular
function! CustomTabularPatterns()
  if exists('g:tabular_loaded')
    AddTabularPattern! symbols         / :/l0
    AddTabularPattern! hash            /^[^>]*\zs=>/
    AddTabularPattern! chunks          / \S\+/l0
    AddTabularPattern! assignment      / = /l0
    AddTabularPattern! comma           /^[^,]*,/l1
    AddTabularPattern! colon           /:\zs /l0
    AddTabularPattern! options_hashes  /:\w\+ =>/
  endif
endfunction

autocmd VimEnter * call CustomTabularPatterns()

" shortcut to align text with Tabular
map <Leader>a :Tabularize<space>

" Zoomwin
map <Leader>z :ZoomWin<CR>

" Unimpaired
" Bubble single lines
nmap <C-Up> [e
nmap <C-Down> ]e

" Bubble multiple lines
vmap <C-Up> [egv
vmap <C-Down> ]egv

" Syntastic
let g:syntastic_enable_signs=1
let g:syntastic_quiet_messages = {'level': 'warnings'}
" syntastic is too slow for haml and sass
let g:syntastic_mode_map = { 'mode': 'active',
                           \ 'active_filetypes': [],
                           \ 'passive_filetypes': ['haml','scss','sass'] }
" Vim Rails
map <Leader>oc :Rcontroller<Space>
map <Leader>ov :Rview<Space>
map <Leader>om :Rmodel<Space>
map <Leader>oh :Rhelper<Space>
map <Leader>oj :Rjavascript<Space>
map <Leader>os :Rstylesheet<Space>
map <Leader>oi :Rintegration<Space>

" vim-surround
" # to surround with ruby string interpolation
let g:surround_35 = "#{\r}"
" - to surround with no-output erb tag
let g:surround_45 = "<% \r %>"
" = to surround with output erb tag
let g:surround_61 = "<%= \r %>"

" ctrl-p
nmap <Leader>t :CtrlP<CR>
nmap <Leader>b :CtrlPBuffer<CR>
nmap <Leader>m :CtrlPBufTag<CR>
nmap <Leader>mm :CtrlPBufTagAll<CR>
nmap <Leader>y :CtrlPBufTagAll<CR>
set wildignore+=*.class

" ctrlp-funky
let g:ctrlp_extensions = ['funky']
nmap <Leader>f :CtrlPFunky<CR>

" set clipboard=unnamed
set tags=./tags,tags,../tags

set mouse=
let g:airline_powerline_fonts = 1
let g:airline_theme             = 'powerlineish'
let g:airline_enable_branch     = 1
let g:airline_enable_syntastic  = 1

" Trailing whitespace
function! TrimWhiteSpace()
    %s/\s\+$//e
endfunction

nnoremap <silent> <Leader>rts :call TrimWhiteSpace()<CR>
set nocompatible
filetype plugin on
